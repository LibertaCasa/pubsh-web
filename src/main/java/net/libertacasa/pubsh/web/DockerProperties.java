package net.libertacasa.pubsh.web;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("lysergic.docker")
public class DockerProperties {

	private static String endpoint;
	
    public void setEndpoint(String endpoint) {
    	DockerProperties.endpoint = endpoint;
    }
    
    public static String getEndpoint() {
        return endpoint;
    }    

}
