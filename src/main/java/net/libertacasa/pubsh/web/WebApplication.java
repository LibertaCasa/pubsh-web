package net.libertacasa.pubsh.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.security.Principal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.IDToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.kagkarlsson.scheduler.Scheduler;
import com.github.kagkarlsson.scheduler.task.TaskInstanceId;

@SpringBootApplication
@Controller
public class WebApplication {
	private static Logger log = LoggerFactory.getLogger(WebApplication.class);
	
	private static Scheduler scheduler;
	
	@Autowired
	public void Scheduler(Scheduler scheduler) {
		WebApplication.scheduler = scheduler;
	}

    static ArrayList<String> availableOs = new ArrayList<String>();
    
	public static void main(String[] args) {
		log.trace("Initializing public shell web service ...");
		SpringApplication.run(WebApplication.class, args);
		
		//to-do: move this to some config file or database
		log.trace("Loading available shell operating systems ...");
        availableOs.add("archlinux");
        availableOs.add("opensuse-leap");
        availableOs.add("opensuse-tumbleweed");
        availableOs.add("ubuntu");		
	}

	@GetMapping("/")
	public String index() {
		log.trace("GET mapping '/' called.");
		return "redirect:/portal";
		
	}
	
	@GetMapping("/admin")
	public String portal(Model model) throws pubshError {
		log.debug("GET mapping '/admin' called.");
		KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken)
        SecurityContextHolder.getContext().getAuthentication();
		Principal principal = (Principal) authentication.getPrincipal();
		String username="";
		String email="";
		String attribute01="";
		
		log.trace("Checking for missing or invalid KeycloakPrincipal ...");
      if (! (principal instanceof KeycloakPrincipal)) {
    	  log.debug("Missing or invalid KeycloakPrincipal, throwing error.");
    	  log.warn("Received logon without Keycloak principal.");
    	  throw new pubshError("Invalid request.");
      }      
      if (principal instanceof KeycloakPrincipal) {
    	  log.debug("Found valid KeycloakPrincipal.");
          KeycloakPrincipal<?> kPrincipal = (KeycloakPrincipal<?>) principal;
          IDToken token = kPrincipal.getKeycloakSecurityContext().getIdToken();
          Map<String, Object> customClaims = token.getOtherClaims();
          log.trace("Checking for missing username claim ...");
          if (! customClaims.containsKey("username")) {
        	  log.debug("Missing username claim, throwing error.");
        	  log.warn("Received logon without username.");        	  
        	  throw new pubshError("Invalid request.");
          }
          log.trace("Checking for email claim ...");
          if (customClaims.containsKey("email")) {
        	  log.debug("Found email claim.");
              email = String.valueOf(customClaims.get("email"));
          }
          log.trace("Checking for username claim ...");
          if (customClaims.containsKey("username")) {
        	  log.debug("Found username claim.");
        	  username = String.valueOf(customClaims.get("username"));
          }
          log.trace("Checking for attribute01 claim ...");
          if (customClaims.containsKey("attribute01")) {
        	  log.debug("Found attribute01 claim.");
        	  attribute01 = String.valueOf(customClaims.get("attribute01"));
          }
          log.trace("Constructing model 1/2 ...");
          model.addAttribute("username", username);
          model.addAttribute("principalid", principal.getName());
          model.addAttribute("email", email);
          model.addAttribute("attribute01", attribute01);
          log.debug("Added to model: username -> {}, principalid -> {}, email -> {}", username, principal.getName(), email);
      }
      
      log.trace("Fetching Docker images ...");
      List<Image> images = Docker.getImages(null);
      log.debug("Fetched Docker images: {}", images);
      log.trace("Fetching Docker containers ...");
      List<Container> containers = Docker.getContainers(null);
      log.debug("Fetched Docker containers: {}", containers);
      log.trace("Initializing custom container Array ...");
      ArrayList<MyContainer> yourContainers = new ArrayList<MyContainer>();
      
      log.trace("Parsing and reconstructing containers ...");
      for (Container container : containers) {
    	  log.debug("Parsing container: {}", container);
    	  try {
	    	  String containername = container.getNames()[0].substring(1);
	    	  String containeruser = containername.split("_")[0];
	    	  log.debug("Found container from {}", containeruser);
	    	  String containerid = container.getId();
	    	  String containerimage = container.getImage();
	    	  String containerimageid = container.getImageId();
	    	  String containerstatus = container.getStatus();
	    	  log.trace("Fetching expiration date ...");
	    	  TaskInstanceId taskid = SchedulerBean.shellRemovalTask().instance(containeruser + "&" + containerid);
	    	  log.debug("Found task: {}", taskid.getId());
	    	  Instant taskexecution;
	    	  if (scheduler.getScheduledExecution(taskid).isPresent()) {
	    		  taskexecution = scheduler.getScheduledExecution(taskid).get().getExecutionTime();
	    		  log.debug("Task execution: {}", scheduler.getScheduledExecution(taskid));
	    	  } else {
	    		  taskexecution = null;
	    	  }
	    	  
	    	  log.trace("Initializing new container object ...");
	    	  MyContainer thisContainer = new MyContainer();
	    	  thisContainer.setName(containername);
	    	  log.trace("Set containername to: {}", containername);
	    	  thisContainer.setId(containerid);
	    	  log.trace("Set containerid to: {}", containerid);
	    	  thisContainer.setImageId(containerimageid);
	    	  log.trace("Set containerimageid to: {}", containerimageid);
	    	  thisContainer.setExpirationDate(taskexecution);
	    	  log.trace("Set expiration date to: {}", taskexecution);
	    	  thisContainer.setImage(containerimage);
	    	  log.trace("Set containerimage to: {}", containerimage);
	    	  thisContainer.setStatus(containerstatus);
	    	  log.trace("Set containerstatus to: {}", containerstatus);
	    	  yourContainers.add(thisContainer);
	    	  log.debug("Constructed new container object: {}", thisContainer);
	  	  } catch (Exception exception) {
			  log.error("Failed to parse container {}", container);
			  log.debug("Caught exception: {}", exception.getMessage());
			  log.trace("STACK TRACE: {}", exception);
		  }
      }
      
      log.trace("Constructing model 2/2 ...");
      model.addAttribute("docker_images", images);
      model.addAttribute("docker_containers", containers); //consider removing, yourContainers should suffice
      model.addAttribute("custom_containers", yourContainers);
      model.addAttribute("availableOs", availableOs);
      log.debug("Added to model: images -> {}, containers -> {}, availableOs -> {}", images, yourContainers, availableOs);
      
      log.trace("Done, returning admin mapper ...");
      return("admin");
	}
	
	@GetMapping("/portal")
	public String user(Model model) throws pubshError {
		log.debug("GET mapping '/portal' called.");
		KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken)
        SecurityContextHolder.getContext().getAuthentication();
		Principal principal = (Principal) authentication.getPrincipal();
		String username="";
		String email="";
		String attribute01="";
		
		log.trace("Checking for missing or invalid KeycloakPrincipal ...");
      if (! (principal instanceof KeycloakPrincipal)) {
    	  log.debug("Missing or invalid KeycloakPrincipal, throwing error.");
    	  log.warn("Received logon without Keycloak principal.");
    	  throw new pubshError("Invalid request.");
      }      
      if (principal instanceof KeycloakPrincipal) {
    	  log.debug("Found valid KeycloakPrincipal.");
          KeycloakPrincipal<?> kPrincipal = (KeycloakPrincipal<?>) principal;
          IDToken token = kPrincipal.getKeycloakSecurityContext().getIdToken();
          Map<String, Object> customClaims = token.getOtherClaims();
          log.trace("Checking for missing username claim ...");
          if (! customClaims.containsKey("username")) {
        	  log.debug("Missing username claim, throwing error.");
        	  log.warn("Received logon without username.");        	  
        	  throw new pubshError("Invalid request.");
          }
          log.trace("Checking for email claim ...");
          if (customClaims.containsKey("email")) {
        	  log.debug("Found email claim.");
              email = String.valueOf(customClaims.get("email"));
          }
          log.trace("Checking for username claim ...");
          if (customClaims.containsKey("username")) {
        	  log.debug("Found username claim.");
        	  username = String.valueOf(customClaims.get("username"));
          }
          log.trace("Checking for attribute01 claim ...");
          if (customClaims.containsKey("attribute01")) {
        	  log.debug("Found attribute01 claim.");
        	  attribute01 = String.valueOf(customClaims.get("attribute01"));
          }
          log.trace("Constructing model 1/2 ...");
          model.addAttribute("username", username);
          model.addAttribute("principalid", principal.getName());
          model.addAttribute("email", email);
          model.addAttribute("attribute01", attribute01);
          log.debug("Added to model: username -> {}, principalid -> {}, email -> {}", username, principal.getName(), email);
      }
      
      log.trace("Fetching Docker images ...");
      List<Image> images = Docker.getImages(null);
      log.debug("Fetched Docker images: {}", images);
      log.trace("Fetching Docker containers ...");
      List<Container> containers = Docker.getContainers(null);
      log.debug("Fetched Docker containers: {}", containers);
      log.trace("Initializing custom container Array ...");
      ArrayList<MyContainer> yourContainers = new ArrayList<MyContainer>();    
      
      log.trace("Parsing and reconstructing containers ...");
      for (Container container : containers) {
    	  log.debug("Parsing container: {}", container);
    	  String containername = container.getNames()[0];   	  
    	  String containerid = container.getId();
    	  String containerimage = container.getImage();
    	  String containerimageid = container.getImageId();
    	  String containerstatus = container.getStatus();
    	  log.trace("Fetching expiration date ...");
    	  TaskInstanceId taskid = SchedulerBean.shellRemovalTask().instance(username + "&" + containerid);
    	  Instant taskexecution = scheduler.getScheduledExecution(taskid).get().getExecutionTime();
    	  
    	  log.trace("Initializing new container object ...");
    	  MyContainer thisContainer = new MyContainer();
    	  thisContainer.setName(containername);
    	  log.trace("Set containername to: {}", containername);
    	  thisContainer.setId(containerid);
    	  log.trace("Set containerid to: {}", containerid);
    	  thisContainer.setImageId(containerimageid);
    	  log.trace("Set containerimageid to: {}", containerimageid);
    	  thisContainer.setExpirationDate(taskexecution);
    	  log.trace("Set expiration date to: {}", taskexecution);
    	  thisContainer.setImage(containerimage);
    	  log.trace("Set containerimage to: {}", containerimage);
    	  thisContainer.setStatus(containerstatus);
    	  log.trace("Set containerstatus to: {}", containerstatus);
    	  yourContainers.add(thisContainer);
    	  log.debug("Constructed new container object: {}", thisContainer);
      }
      
      log.trace("Constructing model 2/2 ...");
      model.addAttribute("docker_images", images);
      model.addAttribute("docker_containers", containers); //should yourContainers suffice for the admin view?
      model.addAttribute("custom_containers", yourContainers);
      model.addAttribute("availableOs", availableOs);
      log.debug("Added to model: images -> {}, containers -> {}, availableOs -> {}", images, yourContainers, availableOs);
      
      log.trace("Done, returning portal mapper ...");
      return("portal");
	}	
	
	@DeleteMapping("/frontend/container/delete/{id}")
	public static String deleteContainer(@PathVariable String id, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		log.debug("DELETE mapping '/frontend/container/delete/{}' called.", id);
	 // [Start] This block should move to a logging method. It's only job is to print user details to the console.
	     KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
	     String username= null;
	     String userid = principal.getName();
	     IDToken token = principal.getAccount().getKeycloakSecurityContext().getIdToken();		
	     Map<String, Object> customClaims = token.getOtherClaims();
	     username = String.valueOf(customClaims.get("username"));
 	 // [End]
	        
		log.info("Container deletion triggered for ID {} by {} ({})", id, userid, username);
		
		log.debug("Issuing Docker container deletion ...");
		Docker.deleteContainer(id);
			
		log.trace("Done, redirecting to admin mapper ...");
		return("redirect:/admin");
	}
	
	@DeleteMapping("/frontend/image/delete/{id}")
	public static String deleteImage(@PathVariable String id, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		log.debug("DELETE mapping '/frontend/image/delete/{}' called.", id);
	// [Start] This block should move to a logging method. It's only job is to print user details to the console.
	     KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
	     String username= null;
	     String userid = principal.getName();
	     IDToken token = principal.getAccount().getKeycloakSecurityContext().getIdToken();		
	     Map<String, Object> customClaims = token.getOtherClaims();
	     username = String.valueOf(customClaims.get("username"));
	 // [End]
	     
	     log.info("Image deletion triggered for ID {} by {} ({})", id, userid, username);
	     
	     log.trace("Attempting image deletion ...");
	     try {
	    	 log.debug("Issuing Docker image deletion ...");
	    	 Docker.deleteImage(id);
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage =  "Success - removed image with ID " + id + "!";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	     } catch (com.github.dockerjava.api.exception.ConflictException exception) {
	    	 log.info("Image conflicts with running container, skipping.");
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage = "Error - Image is still being used by a container.";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	    	 log.debug("Caught exception: {}", exception.getMessage());
	     } catch (Exception exception) {
	    	 log.warn("Failed to delete image.");
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage =  "Error - failed to delete image :-(";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	    	 log.debug("Caught exception: {}", exception.getMessage());
	    	 log.trace("STACK TRACE: {}", exception);
	     }
	     
	     log.trace("Done, redirecting to admin mapper ...");
	     return("redirect:/admin");
	}
	
	@PostMapping(path="/frontend/container/add",consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public static String addContainer(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		log.debug("POST mapping '/frontend/container/add' called.");
	     KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
	     String username = null;
	     String userid = principal.getName();
	     IDToken token = principal.getAccount().getKeycloakSecurityContext().getIdToken();		
	     Map<String, Object> customClaims = token.getOtherClaims();
	     username = String.valueOf(customClaims.get("username"));
	     
	     log.trace("Finding OS selection in request body ...");
	     String os = (String) body.getFirst("osChoice");
	     log.debug("Found OS selection: {}", os);
	     
	     log.info("New container with OS {} requested by {} ({})", os, userid, username);
	     
	     log.trace("Initializing random container number ...");
	     Random rand = new Random();
	     log.trace("Generating random container number ...");
	     Integer randomInt = rand.nextInt(9999999-1111);
	     Integer count = randomInt;
	     log.debug("Generated random container number: {}", count);
	     
	     log.trace("Attempting to build image ...");
	     try {
	    	 log.debug("Issuing Docker image build ...");
	    	 String imageid = Docker.buildImage(username, os, count);
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage =  "Success - built image with ID " + imageid + " and tag sh" + count + "!";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	     } catch (Exception exception) {
	    	 //redirectAttributes.addFlashAttribute("message", "Success!"); //why is this here???
	    	 log.warn("Failed to build image.");
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage =  "Error - failed to build image :-(";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	    	 log.debug("Caught exception: {}", exception.getMessage());
	    	 log.trace("STACK TRACE: {}", exception);
	     }
	     
	     //Docker.createContainer(imageid);
	     
	     log.trace("Done, redirecting to portal mapper ...");
	     return("redirect:/portal");
	}
	
	@PostMapping(path="/frontend/shell/add",consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public static String addShell(@RequestBody MultiValueMap<String, String> body, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		log.debug("POST mapping '/frontend/shell/add' called.");
	     KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
	     String username = null;
	     String userid = principal.getName();
	     IDToken token = principal.getAccount().getKeycloakSecurityContext().getIdToken();		
	     Map<String, Object> customClaims = token.getOtherClaims();
	     username = String.valueOf(customClaims.get("username"));
	     
	     log.trace("Finding OS selection in request body ...");
	     String os = (String) body.getFirst("osChoice");
	     log.debug("Found OS selection: {}", os);
	     log.trace("Finding expiration selection in request body ...");
	     Integer expiry = (Integer) Integer.parseInt(body.getFirst("expiry"));
	     log.debug("Found expiration selection: {}", os);
	     
	     log.info("New shell with OS {} requested by {} ({})", os, userid, username);
	     
	     log.trace("Initializing random container number ...");
	     Random rand = new Random();
	     log.trace("Generating random container number ...");
	     Integer randomInt = rand.nextInt(9999999-1111);
	     Integer count = randomInt;
	     log.debug("Generated random container number: {}", count);
	     
	     log.trace("Attempting to create shell ...");
	     try {
	    	 log.debug("Issuing Docker shell creation ...");
	    	 String containerid = Docker.createShell(username, os, count);
	    	 log.trace("Constructing instance ID ...");
	    	 String instanceid = username + "&" + containerid;
	    	 log.debug("Constructed instance ID: {}", instanceid);
	    	 log.trace("Adding scheduler task for future shell deletion ...");
	    	 scheduler.schedule(SchedulerBean.shellRemovalTask().instance(instanceid), Instant.now().plusSeconds(expiry));
	    	 log.debug("Added scheduler task.");
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage =  "Success - spawned shell " + username + "_" + os + "_" + count + " - internal ID: " + containerid;
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	     } catch (Exception exception) {
	    	 log.warn("Failed to create shell.");
	    	 //redirectAttributes.addFlashAttribute("message", "Success!"); //why is this here???
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage =  "Error - failed to build image :-(";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	    	 log.debug("Caught exception: {}", exception.getMessage());
	    	 log.trace("STACK TRACE: {}", exception);
	     }
	     
	     log.trace("Done, redirecting to portal mapper ...");
	     return("redirect:/portal");
	}
	
	@DeleteMapping("/frontend/shell/delete/{id}")
	public static String deleteShell(@PathVariable String id, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		log.debug("DELETE mapping '/frontend/shell/delete/{}' called.", id);
	 // [Start] This block should move to a logging method. It's only job is to print user details to the console.
	     KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
	     String username= null;
	     String userid = principal.getName();
	     IDToken token = principal.getAccount().getKeycloakSecurityContext().getIdToken();		
	     Map<String, Object> customClaims = token.getOtherClaims();
	     username = String.valueOf(customClaims.get("username"));
 	 // [End]
	        
		log.info("Shell deletion triggered for ID {} by {} ({})", id, userid, username);
		
		log.trace("Attempting to delete shell ...");
		try {
			log.debug("Issuing Docker shell deletion ...");
			Docker.deleteShell(username, id);
			log.trace("Constructing return message ...");
			String returnmessage = "Ok, deleted the shell with container ID " + id;
			log.trace("Adding return message attribute ...");
			redirectAttributes.addFlashAttribute("message", returnmessage);
		} catch (com.github.dockerjava.api.exception.NotFoundException exception) {
			log.warn("Shell/Container not found.");
			log.trace("Constructing return message ...");
	    	 String returnmessage =  "Shell does not exist, maybe it already expired?";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	    	 log.debug("Caught exception: {}", exception.getMessage());
		} catch (Exception exception) {
	    	 log.warn("Failed to delete shell.");
	    	 log.trace("Constructing return message ...");
	    	 String returnmessage =  "Error - failed to delete shell :-(";
	    	 log.trace("Adding return message attribute ...");
	    	 redirectAttributes.addFlashAttribute("message", returnmessage);
	    	 log.debug("Caught exception: {}", exception.getMessage());
	    	 log.trace("STACK TRACE: {}", exception);
		}
		
		log.trace("Done, redirecting to portal mapper ...");
		return("redirect:/portal");
	}	
	
	@GetMapping(path = "/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		log.debug("GET mapping '/logout' called.");
		log.trace("Issuing logout request ...");
	  request.logout();
	  log.debug("Issuing Keycloak OIDC logout ...");
	  keycloakSessionLogout(request);
	  log.trace("Done, returning logout confirmation page ...");
	  return "logout";
	 }
	
   private void keycloakSessionLogout(HttpServletRequest request){
	   log.trace("Invoked keycloakSessionLogout ...");
	   log.trace("Fetching Keycloak security context ...");
      RefreshableKeycloakSecurityContext c = getKeycloakSecurityContext(request);
      log.trace("Fetching Keycloak deployment ...");
      KeycloakDeployment d = c.getDeployment();
      log.debug("Fetched Keycloak deployment: {}", d);
      log.debug("Logging Keycloak session out ...");
      c.logout(d);
      log.trace("Done with Keycloak logout.");
   }

   private RefreshableKeycloakSecurityContext getKeycloakSecurityContext(HttpServletRequest request){
	   log.trace("Invoked query of Keycloak security context ...");
	   Object contextName = request.getAttribute(KeycloakSecurityContext.class.getName());
	   log.trace("Returning: {}", contextName);
	  return (RefreshableKeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
   }
   
   public class pubshError extends Exception
   {
     private static final long serialVersionUID = 1L;

	public pubshError(String message)
     {
       super(message);
     }
   }   
   
}
