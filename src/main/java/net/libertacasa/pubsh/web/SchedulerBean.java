package net.libertacasa.pubsh.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.Instant;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.kagkarlsson.scheduler.Scheduler;
import com.github.kagkarlsson.scheduler.task.Task;
import com.github.kagkarlsson.scheduler.task.helper.Tasks;
//import static com.github.kagkarlsson.scheduler.task.schedule.Schedules.fixedDelay;

@Configuration
public class SchedulerBean {
	private static Logger log = LoggerFactory.getLogger(SchedulerBean.class);

//    @Bean
//    Task<Void> recurringSampleTask(CounterService counter) {
//        return Tasks
//            .recurring("recurring-sample-task", fixedDelay(Duration.ofMinutes(1)))
//            .execute((instance, ctx) -> {
//                System.out.printf("Recurring testing task. Instance: %s, ctx: %s\n", instance, ctx);
//                CounterService.increase();
//            });
//    }

	@Bean
	public static Task<Void> shellRemovalTask() {
		log.debug("Scheduler: shellRemovalTask invoked ...");
	  return Tasks.oneTime("shell-removal")
	      .execute((instance, ctx) -> {
	          log.info("Running container removal task - Instance: {}, ctx: {}", instance, ctx);
	          try {
	        	  log.trace("Constructing shell data ...");
	        	  String username = instance.getId().split("&")[0];
	        	  String containerid = instance.getId().split("&")[1];
	        	  log.debug("Deleting shell with username -> {} and container ID -> {}", username, containerid);
	        	  Docker.deleteShell(username, containerid);
	          } catch (com.github.dockerjava.api.exception.NotFoundException exception) {
	        	  	log.warn("Container does not exist.");
					log.debug("Caught exception: {}", exception.getMessage());
	          } catch (com.github.dockerjava.api.exception.ConflictException exception) {
	 	    	 log.info("Image conflicts with running container, skipping.");
		    	 log.debug("Caught exception: {}", exception.getMessage().replace("\n", ""));
				} catch (Exception exception) {
			    	 log.warn("Failed to delete image.");
			    	 log.debug("Caught exception: {}", exception.getMessage());
			    	 log.trace("STACK TRACE: {}", exception);
			     }
	      });
	}

    @Bean
    Task<Void> sampleOneTimeTask() {
    	log.debug("Scheduler: sampleOneTimeTask invoked ...");
        return Tasks.oneTime("one-time-sample-task")
            .execute((instance, ctx) -> {
                log.info("OK, one-shot testing task.");
            });
    }
    
	// keeping this as a quick way to check if the scheduler booted up after an application restart
    @Bean
    CommandLineRunner executeOnStartup(Scheduler scheduler, Task<Void> sampleOneTimeTask) {
        log.info("Scheduling one-shot testing task to execute now.");

        return ignored -> scheduler.schedule(
            sampleOneTimeTask.instance("command-line-runner"),
            Instant.now()
        );    
   }
}
