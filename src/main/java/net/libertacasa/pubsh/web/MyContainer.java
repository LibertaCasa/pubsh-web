package net.libertacasa.pubsh.web;

import java.io.Serializable;
import java.time.Instant;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class MyContainer implements Serializable {
    private static final long serialVersionUID = 5870179635916284739L;
    
    @Override
    public String toString() {
//    	return ReflectionToStringBuilder.toString(this,ToStringStyle.SHORT_PREFIX_STYLE);
        return new ReflectionToStringBuilder(this,ToStringStyle.SHORT_PREFIX_STYLE).
        	       append("name", name).
        	       append("id", id).
        	       append("ExpirationDate", ExpirationDate).
        	       append("image", image).
        	       append("imageid", imageid).
        	       append("status", status).
        	       toString();
    }
    
	private String name;
	
	private String id;
	
    private Instant ExpirationDate;

    private String image;
    
    private String imageid;    
    
    private String status;
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
    public void setExpirationDate(Instant ExpirationDate) {
    	this.ExpirationDate = ExpirationDate;
    }
    
    public void setImage(String image) {
    	this.image = image;
    }

    public void setImageId(String imageid) {
    	this.imageid = imageid;
    }    
    
    public void setStatus(String status) {
    	this.status = status;
    }
	
    public String getName() {
        return name;
    }
    
    public String getId() {
    	return id;
    }
	
    public Instant getExpirationDate() {
        return ExpirationDate;
    }
    
    public String getImage() {
    	return image;
    }
    
    public String getImageId() {
    	return imageid;
    }    
    
    public String getStatus() {
    	return status;
    }
    
}
